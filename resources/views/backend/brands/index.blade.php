<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Product Brands</title>
  </head>
  <body>
    <h1 class="text-center underline">Brands</h1>

    User : {{auth()->user()->name ?? ''}}

    <button><a href="{{route('brands.create')}}" classs="btn btn-sm btn-primary">Add New Brand</a></button>

    {{-- @dd($brands); --}}

    <table class="table table-bordered">
        <thead class="bg-dark text-white text-center">
            <tr>
                <th class="text-center">Ser No</th>
                <th class="text-center">Name</th>
                <th class="text-center">Status</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>

        <tbody>

            @forelse ($brands as $brand)
                <tr>
                    <td class="text-center">{{$loop->iteration}}</td>
                    <td class="text-center">{{$brand->name}}</td>
                    {{-- <td class="text-center">{{$brand->is_active == 0 ? 'Inactive' : 'Active'}}</td> --}}
                    <td class="text-center">
                        @if ($brand->is_active == 1)
                              <span class="badge bg-success">Active</span>          
                        @else  
                              <span class="badge bg-danger">Inactive</span>  
                        @endif
                    </td>
                    <td class="text-center">
                        @can('brands_edit')
                              <a href="#" class="btn btn-sm btn-primary">Edit</a>
                        @endcan
                        
                        @can('brands_delete')
                              <a href="#" class="btn btn-sm btn-danger">Delete</a>
                        @endcan
                    </td>
                </tr>          
            @empty

              {{-- <p>Brand is empty</p>  --}}
                <tr>
                  <td colspan="3" class="text-center">No Record Found</td>
                </tr>

            @endforelse
        </tbody>

    </table>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>