<?php

namespace App\Policies;

use App\Models\Brand;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BrandPolicy
{
    use HandlesAuthorization;

    
    public function viewAny(User $user)
    {
        
    }

    
    public function view(User $user, Brand $brand)
    {
        
    }

    
    public function create(User $user)
    {
        // $user = auth()->user();

        // if($user->role_id == 1 || $user->role_id == 2)
        if(User::isAdmin())
        {
            return true;
        }
        else{
            return false;
        }
    }

    
    public function edit()
    {
        // $user = auth()->user();

        if(User::isAdmin()||User::isEditor()){
            return true;
        }
        else{
            return false;
        }
    }

    
    public function delete()
    {
        // $user = auth()->user();

            if(User::isAdmin()){
                return true;
            }
            else{
                return false;
            }
    }

    public function restore(User $user, Brand $brand)
    {
        
    }

    
    public function forceDelete(User $user, Brand $brand)
    {
        
    }
}
