<?php

namespace App\Providers;

use App\Policies\BrandPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        'App\Models\Model' => 'App\Policies\BrandPolicy',
    ];

    
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('brands_create', [BrandPolicy::class,'create']);
        // function () {


        //     // dd(auth()->user()->role_id);
        //     // return true;
            


        // });

        Gate::define('brands_delete', [BrandPolicy::class,'delete']);
        // function(){
            
        // });

        Gate::define('brands_edit', [BrandPolicy::class,'edit']); 
        // function(){
            
        // });
    }
}
