<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class BrandController extends Controller
{
    public function index()
    {
        // $brands = Brand::all();
        // dd($brands);

        // $this->authorize('brands_index');

        $brands = DB::table('brands')->get();
        // dd($brands);
        return view('backend.brands.index', ['brands' => $brands]);
    }

    public function create()
    {
        $this->authorize('brands_create');
        return view('backend.brands.create');
    }

    public function store(Request $request)
    {
        // dd("check");
        // dd($request->all());
        try{
            Brand::create([
                'name' => $request->name ?? '',
            ]);

            return redirect()->route('brands.index');
        }
        catch(QueryException $e){
            dd($e->getMessage());
        }
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        
    }

    public function update(Request $request, $id)
    {
        
    }

    public function destroy($id)
    {
        $this->authorize('brands_delete');
    }
}
